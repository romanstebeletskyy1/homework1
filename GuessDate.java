import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class GuessDate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Let the game begin!\n");
        System.out.println("Enter your name: ");

        String name = scanner.nextLine();
        int index = random.nextInt(17);
        int indexQuestion = 1;
        int indexDate = 0;
        int userDate;

        String[][] dates = {{"1939", "When did the World War II begin?"}, {"1945", "When did the World War II end"},
                {"1918", "When did the World War I end?"}, {"1914", "When did the World War I begin?"},
                {"1991", "When did Ukraine become independent?"}, {"2022", "When did russia attack Ukraine?"},
                {"1932", "When the Holodomor began?"}, {"2022", "What year did Messi win the World Cup?"},
                {"1919", "When was West Ukrainian People's Republic formed?"}, {"1922", "When was the USSR formed?"},
                {"1990", "When was the Revolution on Granite?"}, {"2014", "When was the Revolution of Dignity?"},
                {"1963", "When was Kennedy killed?"}, {"1961", "When was the first space flight?"},
                {"988", "When was Rus' baptized?"}, {"1492", "When was America discovered?"},
                {"1996", "When was the hryvnia introduced?"}, {"1986", "When did the Chernobyl disaster happen?"}};

        String variableDate = dates[index][indexDate];
        int date = Integer.parseInt(variableDate);

        System.out.println(name + ',' + " " + dates[index][indexQuestion] + ": ");
        while (true) {
            if (!scanner.hasNextInt()) {
                System.err.println("The number must be an integer!!!\n");
                System.out.println(name + ',' + " " + dates[index][indexQuestion] + ": ");
                scanner.next();
            } else {
                userDate = scanner.nextInt();
                if (userDate == date) {
                    System.out.println("Congratulations, " + name + "!");
                    break;
                } else if (userDate < date) {
                    System.out.println("The event happened later!");
                } else {
                    System.out.println("The event happened earlier!");
                }
            }
        }
    }
}

