import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class NumberGuess {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Let the game begin!\n");
        System.out.println("Enter your name: ");

        String name = scanner.nextLine();
        int number = random.nextInt(101);
        int userNumber;

        int[] guessedNumbers = new int[0];

        System.out.println(name + ',' + " guess a number [0-100]: ");
        while (true) {
            if (!scanner.hasNextInt()) {
                System.err.println("The number must be an integer!!!\n");
                System.out.println(name + ',' + " enter please a number [0-100]: ");
                scanner.next();
            } else {
                userNumber = scanner.nextInt();

                int[] newArray = Arrays.copyOf(guessedNumbers, guessedNumbers.length + 1);
                newArray[guessedNumbers.length] = userNumber;
                guessedNumbers = newArray;

                if (userNumber > 100 || userNumber < 0) {
                    System.err.println(name + ',' + " the number must be in the range [0-100]: ");
                } else {
                    if (userNumber == number) {
                        System.out.println("Congratulations, " + name + "!");

                        Arrays.sort(guessedNumbers);
                        System.out.println("Your numbers: ");
                        for (int i = guessedNumbers.length-1; i>=0; i--) {
                            System.out.print(guessedNumbers[i]+" ");
                        }
                        break;
                    } else if (userNumber < number) {
                        System.out.println("Your number is too small. Please, try again.");
                    } else {
                        System.out.println("Your number is too big. Please, try again.");
                    }
                }
            }
        }

    }
}